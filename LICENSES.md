# OTRS LICENCES #

This page contains all the licenses used by the OTRS application.

## CPAN CGI ##
Web: http://stein.cshl.org/WWW/software/CGI/

License:
```
Copyright 1995-1998 Lincoln D. Stein. 
All rights reserved. It may be used and modified freely, 
but I do request that this copyright notice remain attached to the file.
You may modify this module as you wish, but if you redistribute 
a modified version, please attach a note listing the modifications you have made.
```

## CPAN Apache::DBI ##
Web:     n/a

License: 
```
Copyright (C) 2019 Arvato Systems

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

## Name: CPAN Apache2::Reload ##
Web:     n/a

License: 
```
Copyright (C) 2019 Arvato Systems

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

## Name: CPAN Algorithm::Diff ##
Web:     n/a

License: 
```
Copyright (C) 2019  Arvato Systems

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.
```
Note:    Parts Copyright (c) 2000-2004 Ned Konz. All rights reserved. Parts by Tye McQueen.


